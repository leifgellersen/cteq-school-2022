#!/bin/bash

# Set the Docker and local notebook directories.
DOCKERDIR=/usr/local/share/jupyter
LOCALDIR=$PWD/tutorial
STARTDIR=$LOCALDIR/.ipython/profile_default/startup/

# Create local notebook directory and copy notebooks.
mkdir -p $LOCALDIR
cp -n $DOCKERDIR/notebooks/*.ipynb $LOCALDIR

# Copy the Pythia documentation, examples, and figures.
cp -nr /usr/local/share/Pythia8/htmldoc $LOCALDIR
cp -nr /usr/local/share/Pythia8/pdfdoc $LOCALDIR
cp -nr /usr/local/share/Pythia8/examples $LOCALDIR
cp -nr $DOCKERDIR/figures $LOCALDIR

# Create the default IPython startup.
mkdir -p $STARTDIR
cp -n $DOCKERDIR/startup/00-wurlitzer.py $STARTDIR

# Create and set the Jupyter configuration.
mkdir -p $LOCALDIR/.jupyter
export JUPYTER_CONFIG_DIR=$LOCALDIR/.jupyter
export JUPYTER_PATH=$LOCALDIR/.jupyter
export JUPYTER_RUNTIME_DIR=$LOCALDIR/.jupyter
export HOME=$LOCALDIR

# Trust the Jupyter notebooks.
jupyter trust $LOCALDIR/*.ipynb

# Start Jupyter.
jupyter notebook --no-browser --allow-root --ip 0.0.0.0 --port 8888 $LOCALDIR
